/* GGC - Pluggable garbage collector for GLib programs
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "ggc.h"
#define GC_DEBUG
#include "gc.h"

#include <string.h>
#include <stdlib.h>

#include <config.h>

/* This backtrace scariness is because we want to pass some useful
 * notation about where a block originates in to the debug collector,
 * so it can print a useful message if it finds leaks.
 */

#ifdef USE_BACKTRACE
#include <execinfo.h>

static char**
get_backtrace (void)
{
  void *bt[500];
  int bt_size;
  char **syms;
  char *s;
  char *end;
  
  bt_size = backtrace (bt, 10);

  syms = backtrace_symbols (bt, bt_size);

  /* since syms is a flat memory block, convert all
   * nul-termination but the last into a comma so we get
   * a full backtrace as a single string.
   */
  end = strchr (syms[bt_size-1], '\0');
  s = syms[0];

  while (s != end)
    {
      if (*s == '\0')
        *s = ',';
      ++s;
    }
  
  return syms;
}
#else
static char**
get_backtrace (void)
{
  return NULL;
}
#endif

static gpointer
ggc_try_malloc (gsize n_bytes)
{
  char **syms;
  gpointer ret;
  
  syms = get_backtrace ();

  /* we store syms at the start of the block, so
   * we can free them again.
   */
  ret = GC_debug_malloc (n_bytes + sizeof (syms),
                         syms ? syms[0] : "(unknown)", 0);

  (*(char***)ret) = syms;

  ret += sizeof (syms);
  
  return ret;
}

static gpointer
ggc_malloc (gsize n_bytes)
{
  return ggc_try_malloc (n_bytes);
}

static gpointer
ggc_calloc (gsize n_blocks,
            gsize n_block_bytes)
{
  /* GC_malloc clears the block */
  return ggc_try_malloc (n_block_bytes * n_blocks);
}

static void
ggc_free (gpointer mem)
{
  char **syms;
  gpointer real;
  
  real = mem - sizeof (syms);

  syms = (*(char***)real);  
  
  GC_debug_free (real);

  /* Of course we won't get here if the memory gets collected
   * instead of g_free()'d, but since this is a leak checker,
   * we expect g_free() to happen.
   */
  if (syms)
    free (syms);
}

static gpointer
ggc_try_realloc (gpointer mem,
                 gsize    n_bytes)
{
  char **syms;
  gpointer real;
  gpointer ret;

  if (mem == NULL)
    return ggc_try_malloc (n_bytes);
  
  real = mem - sizeof (syms);

  syms = (*(char***)real);  
  
  ret = GC_debug_realloc (real, n_bytes + sizeof (syms),
                          syms ? syms[0] : "(unknown)", 0);

  ret += sizeof (syms);
  
  return ret;
}

static gpointer
ggc_realloc (gpointer mem,
             gsize    n_bytes)
{
  if (mem == NULL)
    return ggc_try_malloc (n_bytes);
  
  return ggc_try_realloc (mem, n_bytes);
}

static GMemVTable ggc_table = {
  ggc_malloc,
  ggc_realloc,
  ggc_free,
  ggc_calloc,
  ggc_try_malloc,
  ggc_try_realloc
};

static void
warn_proc (char *msg, GC_word arg)
{
  g_warning (msg, (unsigned long)arg);
}

static gboolean initted = FALSE;

void
ggc_debug_init (GgcFlags flags)
{
  if (!initted)
    {
      GC_find_leak = 1;      
      GC_set_warn_proc (warn_proc);

      if (flags & GGC_INCREMENTAL)
        GC_enable_incremental ();
      
      g_mem_set_vtable (&ggc_table);
      initted = TRUE;
    }
}

void
ggc_debug_check_leaks (void)
{
  if (initted)
    GC_gcollect ();
}
