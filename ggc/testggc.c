/* GGC - Pluggable garbage collector for GLib programs
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "ggc.h"

gboolean
allocate_stuff (gpointer data)
{
  guchar *mem;
  guchar *end;
  int i;
  
  g_malloc (5);
  g_try_malloc (10);
  g_realloc (NULL, 7);

  mem = g_malloc0 (14);

  /* An example of something that can fail if
   * you don't consider a pointer inside the "mem" block
   * a reference to the block, I think
   */
  end = mem + 14;
  while (mem != end)
    {
      g_assert (*mem == '\0');

      ++mem;
    }

  i = 0;
  while (i < 300)
    {
      g_malloc (i);
      ++i;
    }

  return TRUE;
}

int
main (int    argc,
      char **argv)
{
  GMainLoop *loop;

  /* If you remove this line or GGC is in a bad mood,
   * running the test program will implode your system ;-)
   */
  ggc_init (0);
  
  loop = g_main_loop_new (NULL, FALSE);

  g_idle_add (allocate_stuff, NULL);
  
  g_main_loop_run (loop);

  return 0;
}
