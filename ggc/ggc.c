/* GGC - Pluggable garbage collector for GLib programs
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "ggc.h"
#include "gc.h"

static gpointer
ggc_try_malloc (gsize n_bytes)
{
  return GC_malloc (n_bytes);
}

static gpointer
ggc_malloc (gsize n_bytes)
{
  return GC_malloc (n_bytes);
}

static gpointer
ggc_calloc (gsize n_blocks,
            gsize n_block_bytes)
{
  /* GC_malloc clears the block */
  return GC_malloc (n_block_bytes * n_blocks);
}

static void
ggc_free (gpointer mem)
{
  /* For efficiency, we would free here, to take
   * advantage of code that does use free. But
   * for now I'm playing around and want to see if
   * the collector works. ;-)
   */
}

static gpointer
ggc_try_realloc (gpointer mem,
                 gsize    n_bytes)
{
  return GC_realloc (mem, n_bytes);
}

static gpointer
ggc_realloc (gpointer mem,
             gsize    n_bytes)
{
  return GC_realloc (mem, n_bytes);
}

static GMemVTable ggc_table = {
  ggc_malloc,
  ggc_realloc,
  ggc_free,
  ggc_calloc,
  ggc_try_malloc,
  ggc_try_realloc
};

void
ggc_init (GgcFlags flags)
{
  static gboolean initted = FALSE;

  if (!initted)
    {
      if (flags & GGC_INCREMENTAL)
        GC_enable_incremental ();
      
      g_mem_set_vtable (&ggc_table);
      initted = TRUE;
    }
}

gboolean
ggc_collect_increment (void)
{
  return GC_collect_a_little () != FALSE;
}
