/* GGC - Pluggable garbage collector for GLib programs
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GGC_H__
#define __GGC_H__

#include <glib.h>

typedef enum
{
  GGC_INCREMENTAL
} GgcFlags;

void ggc_init (GgcFlags flags);

gboolean ggc_collect_increment (void);

void ggc_debug_init (GgcFlags flags);
void ggc_debug_check_leaks (void);

#endif /* __GGC_H__ */
